from tastypie.resources import ModelResource
from tastypie.paginator import Paginator
from cal.models import Cal, Event
from tastypie.authorization import Authorization
from tastypie.authentication import ApiKeyAuthentication
from tastypie.fields import ToOneField


class CalResource(ModelResource):
	class Meta:
		queryset = Cal.objects.all()	
		paginator_class = Paginator
		authorization = Authorization()
	def apply_authorization_limits(self, request, object_list):
		return object_list.filter(user=request.SESSION['username'])

 
class EventResource(ModelResource):
	cid = ToOneField(CalResource, "cal", full=True)
	class Meta:
		authorization = Authorization()
		authorization.apply_limits()
		queryset = Event.objects.all()
		paginator_class = Paginator
		always_return_data = True
	def apply_authorization_limits(self, request, object_list):
		return object_list.filter(cal_id=1)
	
	def dehydrate_cid(self, bundle):
		return bundle.obj.cal.id

	def hydrate_cid(self, bundle):
		bundle.data["cid"] = "/cal/%d/" % bundle.data["cid"]
		return bundle
	
	def hydrate_id(self, bundle):
		if bundle.data["id"] == 0:
			bundle.data["id"] = None
		return bundle
	

	