
from django.conf.urls import patterns, include, url
from cal.api import CalResource, EventResource
 
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

# RESTful setup:
cal_resource = CalResource()
event_resource = EventResource()
 
urlpatterns = patterns('',
    # Admin page
    url(r'^admin/', include(admin.site.urls)),
 
    # Templates
    url(r'^', include('cal.urls', namespace="cal")),
 
    # RESTful URLs
    url(r'^', include(cal_resource.urls)),
    url(r'^', include(event_resource.urls)),
)
